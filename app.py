#!/usr/bin/env python3
from aws_cdk import core
from cdk.web import WebStack
from cdk.vpc import VpcStack

ENVS = ['dev']
REGION ='us-east-1'
ACCOUNT = {
    'dev': '485030723725'
}
APP_NAME='pro-host'
CIDR = {
    'dev': '10.43.0.0/16'
}
NAT_GATEWAY = {
    'dev': 1,
}

cdk_env = core.Environment(account=ACCOUNT['dev'], region=REGION)

app = core.App()

aws_env = app.node.try_get_context('env')
web_ami = app.node.try_get_context('web_ami')

non_prod_zone_name = f'{aws_env}.cxi.mindgrb.io'
domain_name = f'app.{aws_env}.cxi.mindgrb.io'
api_domain = f'target.{aws_env}.cxi.mindgrb.io'
api_port = 5000

if aws_env not in ENVS or not web_ami:
    exit('No environment context')

#
# VPC Stack
# Output: vpcStack.vpc
#
vpcStack = VpcStack(app, f'{APP_NAME}-vpc',
    app_name=APP_NAME,
    cidr=CIDR[aws_env],
    nat_count=NAT_GATEWAY[aws_env],
    env=cdk_env
)

#
# Web Stack
#
webStack = WebStack(app,
    f'{APP_NAME}-{aws_env}-web',
    app_name=APP_NAME,
    web_ami=web_ami,
    vpc=vpcStack.vpc,
    non_prod_zone_name=non_prod_zone_name,
    domain_name=domain_name,
    region=REGION,
    api_port=api_port,
    api_domain=api_domain,
    env=cdk_env
)

app.synth()
