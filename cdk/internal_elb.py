from aws_cdk import (core, aws_ec2, aws_route53,
    aws_certificatemanager, aws_elasticloadbalancingv2, aws_route53_targets,
    aws_elasticloadbalancingv2_targets
)

class InternalELB(core.Construct):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:

        app_name = kwargs.pop('app_name', None)
        vpc = kwargs.pop('vpc', None)
        non_prod_zone_name = kwargs.pop('non_prod_zone_name', None)
        domain_name = kwargs.pop('domain_name', None)
        region = kwargs.pop('region', None)
        web_inst = kwargs.pop('web_inst', None)
        api_port = kwargs.pop('api_port', None)

        super().__init__(scope, id, **kwargs)

        #
        # Cert
        #
        zone = aws_route53.HostedZone.from_lookup(self,
            'int-zone',
            domain_name=non_prod_zone_name
        )

        cert = aws_certificatemanager.DnsValidatedCertificate(self,
            'int-cert',
            domain_name=domain_name,
            hosted_zone=zone
        )

        #
        # Application Load Balancer
        #
        lb_sec_group = aws_ec2.SecurityGroup(self, 'int-sg',
            vpc=vpc,
            description=f'{app_name} internal load balancer security group',
            allow_all_outbound=True
        )

        lb_sec_group.add_ingress_rule(
            aws_ec2.Peer.ipv4(vpc.vpc_cidr_block),
            aws_ec2.Port.all_tcp(),
            description='vpc-in'
        )

        load_balancer = aws_elasticloadbalancingv2.ApplicationLoadBalancer(self,
            'int-alb',
            http2_enabled=True,
            vpc=vpc,
            internet_facing=False,
            security_group=lb_sec_group,
            load_balancer_name=f'{app_name}-internal',
            vpc_subnets=aws_ec2.SubnetSelection(
                subnets=vpc.private_subnets
            )
        )


        https_listener = load_balancer.add_listener(
            'int-https-listener',
            certificate_arns=[
                cert.certificate_arn
            ],
            port=443,
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTPS,
            ssl_policy=aws_elasticloadbalancingv2.SslPolicy.TLS12
        )

        https_listener.add_fixed_response(
            'int-https-default',
            content_type=aws_elasticloadbalancingv2.ContentType.TEXT_PLAIN,
            message_body='Not Found',
            status_code='404'
        )

        http_listener = load_balancer.add_listener(
            'int-alb-http-listener',
            port=80,
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTP
        )

        http_listener.add_redirect_response(
            'int-http-redirect',
            status_code='HTTP_301',
            protocol='HTTPS',
            port='443',
        )

        aws_route53.ARecord(self, 'int-dns-alb',
            record_name=domain_name,
            target=aws_route53.RecordTarget.from_alias(
                aws_route53_targets.LoadBalancerTarget(load_balancer)
            ),
            zone=zone
        )

        #
        # Add instance to ALB Target Group
        #
        target_group = aws_elasticloadbalancingv2.ApplicationTargetGroup(self,
            'int-target-group',
            port=api_port,
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTP,
            health_check=aws_elasticloadbalancingv2.HealthCheck(
                enabled=True,
                healthy_http_codes='200,202',
                healthy_threshold_count=5,
                path='/test',
                port=str(api_port),
                protocol=aws_elasticloadbalancingv2.Protocol.HTTP
            ),
            target_group_name=f'{app_name}-internal',
            target_type=aws_elasticloadbalancingv2.TargetType.INSTANCE,
            vpc=vpc,
            targets=[
                aws_elasticloadbalancingv2_targets.InstanceTarget(
                    instance=web_inst,
                    port=api_port
                )
            ],
        )
        https_listener.add_target_groups(
            'int-attach-tg',
            target_groups=[target_group],
            host_header=domain_name,
            priority=1
        )
