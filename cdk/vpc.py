from aws_cdk import (core, aws_ec2, aws_logs)

class VpcStack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:

        app_name = kwargs.pop('app_name', None)
        cidr = kwargs.pop('cidr', None)
        nat_count = kwargs.pop('nat_count', None)

        super().__init__(scope, id, **kwargs)

        flow_logs = aws_logs.LogGroup(self, 'flow-logs',
            log_group_name=f'{app_name}-flow-logs',
            removal_policy=core.RemovalPolicy.DESTROY,
            retention=aws_logs.RetentionDays.ONE_MONTH
        )

        vpc = aws_ec2.Vpc(self, 'vpc',
            cidr=cidr,
            nat_gateway_provider=aws_ec2.NatProvider.gateway(),
            nat_gateways=nat_count,
            max_azs=2,
            nat_gateway_subnets=aws_ec2.SubnetSelection(
                subnet_type=aws_ec2.SubnetType.PUBLIC
            ),
            subnet_configuration= [
                {
                    'name': f'{app_name}-public',
                    'subnetType': aws_ec2.SubnetType.PUBLIC
                },
                {
                    'name': f'{app_name}-private',
                    'subnetType': aws_ec2.SubnetType.PRIVATE
                }
            ],
            flow_logs={
                'All': aws_ec2.FlowLogOptions(
                    destination=aws_ec2.FlowLogDestination.to_cloud_watch_logs(
                        log_group=flow_logs
                    ),
                    traffic_type=aws_ec2.FlowLogTrafficType.ALL
                )
            }
        )

        core.CfnOutput(self,
            'vpc_id',
            value=vpc.vpc_id,
            export_name=f'{app_name}-vpc-id'
        )

        self.vpc = vpc
