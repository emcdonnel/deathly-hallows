from aws_cdk import (core, aws_ec2, aws_iam, aws_logs, aws_route53,
    aws_certificatemanager, aws_elasticloadbalancingv2, aws_route53_targets,
    aws_elasticloadbalancingv2_targets
)
from . import (internal_elb)

class WebStack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:

        app_name = kwargs.pop('app_name', None)
        web_ami = kwargs.pop('web_ami', None)
        vpc = kwargs.pop('vpc', None)
        non_prod_zone_name = kwargs.pop('non_prod_zone_name', None)
        domain_name = kwargs.pop('domain_name', None)
        region = kwargs.pop('region', None)
        api_port = kwargs.pop('api_port', None)
        api_domain = kwargs.pop('api_domain', None)

        web_type = 't3.small'

        super().__init__(scope, id, **kwargs)

        #
        # Cert
        #
        zone = aws_route53.HostedZone.from_lookup(self, 'zone',
            domain_name=non_prod_zone_name
        )

        cert = aws_certificatemanager.DnsValidatedCertificate(self, 'cert',
            domain_name=domain_name,
            hosted_zone=zone
        )

        #
        # Application Load Balancer
        #
        lb_sec_group = aws_ec2.SecurityGroup(self, 'alb-sg',
            vpc=vpc,
            description=f'{app_name} load balancer security group',
            allow_all_outbound=True
        )

        lb_sec_group.add_ingress_rule(
            aws_ec2.Peer.ipv4('0.0.0.0/0'),
            aws_ec2.Port(
                protocol=aws_ec2.Protocol.TCP,
                from_port=80,
                to_port=80,
                string_representation="http-web"
            ),
            description='http-web'
        )

        lb_sec_group.add_ingress_rule(
            aws_ec2.Peer.ipv4('0.0.0.0/0'),
            aws_ec2.Port(
                protocol=aws_ec2.Protocol.TCP,
                from_port=443,
                to_port=443,
                string_representation="https-web"
            ),
            description='https-web'
        )

        load_balancer = aws_elasticloadbalancingv2.ApplicationLoadBalancer(self,
            'alb',
            http2_enabled=True,
            vpc=vpc,
            internet_facing=True,
            security_group=lb_sec_group,
            load_balancer_name=f'{app_name}-alb',
            vpc_subnets=aws_ec2.SubnetSelection(
                subnets=vpc.public_subnets
            )
        )

        https_listener = load_balancer.add_listener(
            'https-listener',
            certificate_arns=[
                cert.certificate_arn
            ],
            port=443,
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTPS,
            ssl_policy=aws_elasticloadbalancingv2.SslPolicy.TLS12
        )

        https_listener.add_fixed_response(
            'https-default',
            content_type=aws_elasticloadbalancingv2.ContentType.TEXT_PLAIN,
            message_body='Not Found',
            status_code='404'
        )

        http_listener = load_balancer.add_listener(
            'alb-http-listener',
            port=80,
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTP
        )

        http_listener.add_redirect_response(
            'http-redirect',
            status_code='HTTP_301',
            protocol='HTTPS',
            # host='#{host}',
            # path='#{path}',
            port='443',
            # query='#{query}'
        )

        aws_route53.ARecord(self, 'dns-alb',
            record_name=domain_name,
            target=aws_route53.RecordTarget.from_alias(
                aws_route53_targets.LoadBalancerTarget(load_balancer)
            ),
            zone=zone
        )

        #
        # Web Instance
        #
        sg = aws_ec2.SecurityGroup(self, 'web-sg',
            vpc=vpc,
            description=f'{app_name} web security group',
            allow_all_outbound=True
        )

        sg.add_ingress_rule(
            aws_ec2.Peer.ipv4('50.225.2.158/32'),
            aws_ec2.Port(
                protocol=aws_ec2.Protocol.TCP,
                from_port=3389,
                to_port=3389,
                string_representation="rdp-from-mg"
            ),
            description='rdp-from-mg'
        )

        sg.connections.allow_from(
            other=lb_sec_group,
            port_range=aws_ec2.Port(
                protocol=aws_ec2.Protocol.TCP,
                from_port=80,
                to_port=80,
                string_representation="alb-to-web"
            )
        )

        sg.add_ingress_rule(
            aws_ec2.Peer.ipv4(vpc.vpc_cidr_block),
            aws_ec2.Port.all_tcp(),
            description='vpc-in'
        )

        inst_logs = aws_logs.LogGroup(self, 'web-logs',
            log_group_name=f'{app_name}-web-logs',
            removal_policy=core.RemovalPolicy.DESTROY,
            retention=aws_logs.RetentionDays.ONE_MONTH
        )

        inst_role = aws_iam.Role(self, 'web-role',
            assumed_by=aws_iam.ServicePrincipal("ec2.amazonaws.com"),
            managed_policies=[
                aws_iam.ManagedPolicy.from_aws_managed_policy_name(
                    "AmazonSSMManagedInstanceCore"
                ),
                aws_iam.ManagedPolicy.from_managed_policy_arn(self,
                    'EC2InstanceConnect-policy-arn',
                    managed_policy_arn='arn:aws:iam::aws:policy/EC2InstanceConnect'
                )
            ],
            inline_policies={
                "logs": aws_iam.PolicyDocument(
                    statements=[
                        aws_iam.PolicyStatement(
                            effect=aws_iam.Effect.ALLOW,
                            actions=[
                                "logs:CreateLogGroup",
                                "logs:CreateLogStream",
                                "logs:PutLogEvents",
                                "logs:DescribeLogStreams"
                            ],
                            resources=[inst_logs.log_group_arn]
                        )
                    ]
                ),
                "s3": aws_iam.PolicyDocument(
                    statements=[
                        aws_iam.PolicyStatement(
                            effect=aws_iam.Effect.ALLOW,
                            actions=[
                                "s3:*"
                            ],
                            resources=["*"]
                        )
                    ]
                )
            }
        )

        inst_image = aws_ec2.MachineImage.generic_linux({
            region: web_ami
        })

        user_data = aws_ec2.UserData.for_windows()
        user_data.add_commands(
            "Install-WindowsFeature -name Web-Server -IncludeManagementTools",
            "Install-WindowsFeature Net-Framework-Core",
            "New-Item -ItemType Directory -Path C:\ -Name HelloWorld",
            "New-Item -ItemType File -Path C:\HelloWorld -Name index.html",
            "Add-Content -Value '<body><h1>Hello World!</h1></body>' -Path C:\HelloWorld\index.html -Encoding UTF8",
            "Import-Module WebAdministration",
            "Set-ItemProperty 'IIS:\Sites\Default Web Site' -Name physicalPath -Value 'C:\HelloWorld'",
        )

        web = aws_ec2.Instance(self, 'web',
            instance_type=aws_ec2.InstanceType(web_type),
            vpc=vpc,
            vpc_subnets=aws_ec2.SubnetSelection(
                subnets=vpc.public_subnets
            ),
            machine_image=inst_image,
            role=inst_role,
            security_group=sg,
            user_data=user_data,
            key_name="windows-container"
        )

        #
        # Add instance to ALB Target Group
        #
        target_group = aws_elasticloadbalancingv2.ApplicationTargetGroup(self,
            'target-group',
            port=80,
            health_check=aws_elasticloadbalancingv2.HealthCheck(
                enabled=True,
                healthy_http_codes='200,202',
                healthy_threshold_count=5,
                path='/',
                port='80'
            ),
            target_group_name=f'{app_name}-web',
            target_type=aws_elasticloadbalancingv2.TargetType.INSTANCE,
            vpc=vpc,
            targets=[
                aws_elasticloadbalancingv2_targets.InstanceTarget(
                    instance=web,
                    port=80
                )
            ],
        )
        https_listener.add_target_groups(
            'attach-tg',
            target_groups=[target_group],
            host_header=domain_name,
            priority=1
        )

        api_elb = internal_elb.InternalELB(self, 'api-elb',
            app_name=app_name,
            vpc=vpc,
            non_prod_zone_name=non_prod_zone_name,
            domain_name=api_domain,
            region=region,
            web_inst=web,
            api_port=api_port
        )
